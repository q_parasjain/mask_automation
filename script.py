import SimpleITK as sitk
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage.interpolation import rotate
from memory_profiler import profile
import time
from tabulate import tabulate

np.set_printoptions(threshold=np.inf)
# print(rotated.shape)
# print(imgarr)
# imgarr=np.transpose(imgarr)
# print(imgarr.shape)
# plt.figure(figsize=(5,5))
# plt.imshow(imgarr[15],  cmap='gray')
# plt.show()

def get_max_in_row(row):
    max_count,last_max=0,0
    for i in row:
        if i==1:
            max_count+=1
            if last_max<max_count:
                last_max=max_count
        else:
            max_count=0
    return last_max

def get_max_in_slice(slice):
    max_in_slice = []
    for row in slice:#one slice
        max_in_slice.append(get_max_in_row(row))
    return max(max_in_slice)

def get_max_in_slice_with_rotation(imgarr,slice_no):
    max_in_slice_with_rotation =[]
    for angle_ in range(0,90,rotation_angle):#rotate
        rotated = rotate(imgarr[slice_no], angle=angle_)
        val = get_max_in_slice(rotated)
        max_in_slice_with_rotation.append(val)
        if val ==0:break
    print(max_in_slice_with_rotation[-1])
    return max(max_in_slice_with_rotation)

@profile
def get_results(rotation_angle):
    max_in_xyz = []
    path = '1.2.124.113540.0.201302211221.3.489703_intracranial_hyperdensities.nii.gz'
    img = sitk.ReadImage(path)
    imgarr = sitk.GetArrayFromImage(img)
    img_shape = imgarr.shape

    for i in range(3):
        max_in_a_direction=[]
        for slice_no in range(img_shape[i]):#slice
            max_in_a_direction.append(get_max_in_slice_with_rotation(imgarr,slice_no))

        print(max_in_a_direction)    
        max_in_xyz.append(max(max_in_a_direction))
        imgarr = imgarr.transpose(2, 0, 1)
    print(max_in_xyz)
    return max_in_xyz
my_list = [1, 2, 3, 5, 6, 9, 10, 15, 18, 30, 45, 90]
lol=[]
for rotation_angle in my_list[::-1]:
    start_time = time.time()
    results = get_results(rotation_angle)
    time_taken = time.time()-start_time

    file1=open("results.txt","a")
    file1.write(tabulate([[results, rotation_angle,time_taken]], tablefmt='orgtbl')+"\n" )
    
    # Release used resources
    file1.close()
    lol.append([results, rotation_angle,time_taken])